#+TITLE: Subscribe for Updates
#+DESCRIPTION: Use this page to subscribe to my newsletter or get the address of my RSS feed.
#+KEYWORDS: subscribe, updates, newsletter, rss, feed
#+DATE: <2018-10-25 Thu>
#+SETUPFILE: ~/org/personal/assets/options.orgmode

If you use a feed reader, getting the latest updates is easy. Just add
[[/blog/index.rss][my feed]] and your reader will do the rest.

* Newsletter

I also have a newsletter hosted by [[https://getrevue.co][Revue]]
called [[https://www.getrevue.co/profile/matthewgraybosch/]["A Day Job
and a Dream"]]. Updates are infrequent (no more than once a month), so
it shouldn't be too annoying.

#+BEGIN_EXPORT html
<div id="revue-embed" class="revue-embed">
  <form action="https://www.getrevue.co/profile/matthewgraybosch/add_subscriber"
	method="post"
	id="revue-form"
	name="revue-form"
	target="_blank">
    <fieldset class="review-form-fieldset">
      <div class="revue-form-group">
	<label class="revue-form-label"
	       for="member_email">Email address</label>
	<input class="revue-form-field"
	       placeholder="Your email address..."
	       name="member[email]"
	       id="member_email"
	       type="email">
      </div>
      <div class="revue-form-group">
	<label class="revue-form-label"
	       for="member_first_name">First name <span class="optional">(Optional)</span></label>
	<input class="revue-form-field"
	       placeholder="First name... (Optional)"
	       name="member[first_name]"
	       id="member_first_name" type="text">
      </div>
      <div class="revue-form-group">
	<label class="revue-form-label"
	       for="member_last_name">Last name <span class="optional">(Optional)</span></label>
	<input class="revue-form-field"
	       placeholder="Last name... (Optional)"
	       name="member[last_name]"
	       id="member_last_name"
	       type="text">
      </div>
      <div class="revue-form-actions">
	<input value="Subscribe"
	       name="member[subscribe]"
	       id="member_submit"
	       type="submit">
      </div>
    </fieldset>
  </form>
</div>
#+END_EXPORT
